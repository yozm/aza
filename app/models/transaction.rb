class Transaction < ApplicationRecord
  #Validations
  validates_presence_of :date_of_transaction, :trans_id, :customer_id, :input_amount, :input_currency, :output_amount, :output_currency
  validates_uniqueness_of :trans_id
end
