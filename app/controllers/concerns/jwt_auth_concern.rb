module JwtAuthConcern
  extend ActiveSupport::Concern

  # JWT token methods
  def jwt_authenticate
    begin
      @decoded_token = JWT.decode(token, Rails.application.credentials.session_secret, true,
                                  { :algorithm => 'HS256' })
      unless @decoded_token.first['expire'] > Time.now.to_i
        render json: { error: "Token expired" }, status: :unauthorized
      end
    rescue JWT::DecodeError
      render json: { error: "Invalid Token" }, status: :unauthorized
    end
  end

  def issue_token(user)
    JWT.encode({ user_id: user.id, expire: 5.minutes.since.to_i },
               Rails.application.credentials.session_secret, 'HS256')
  end

  def token
    request.headers['Authorization']
  end
end