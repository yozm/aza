class UsersController < ApplicationController
  ##TODO Use basic auth for username and password
  def generate_token
    body = JSON.parse(request.body.read)

    user = User.find_by(username: body['username'])

    unless user
      render json: { error: "User not found" }, status: :unauthorized
      return
    end

    password_match = user.authenticate_password(body['password'])

    unless password_match
      render json: { error: "Invalid Password" }, status: :unauthorized
      return
    end

    render json: {
        token: issue_token(user)
    }, status: :created
  end
end