class TransactionsController < ApplicationController
  before_action :jwt_authenticate
  ##TODO Data validation for create and update
  ##TODO DO Return appropriate error for missing transaction in update method
  def create
    transaction_data = JSON.parse(request.body.read)
    transaction = Transaction.new(trans_id: transaction_data['trans_id'],
                                  date_of_transaction: transaction_data['date_of_transaction'].to_date,
                                  customer_id: transaction_data['customer_id'],
                                  input_amount: transaction_data['input_amount'].to_f.round(2),
                                  input_currency: transaction_data['input_currency'],
                                  output_amount: transaction_data['output_amount'].to_f.round(2),
                                  output_currency: transaction_data['output_currency'])
    if transaction.save
      render json: transaction, status: :created
    else
      render json: { error: transaction.errors.full_messages.to_sentence.downcase }, status: :unprocessable_entity
    end
  end

  def index
    @transactions = Transaction.all.order("created_at ASC")
    render json: @transactions, status: :ok
  end

  def show
    @transaction = Transaction.find_by_trans_id(params[:id])
    if @transaction
      render json: @transaction, status: :ok
    else
      render json: { error: 'Invalid Transaction ID'}, status: :not_found
    end
  end

  def update
    transaction_data = JSON.parse(request.body.read)
    @transaction = Transaction.find_by_trans_id(params[:id])
    if @transaction
      @transaction.input_amount = transaction_data['input_amount'].to_f.round(2) if transaction_data['input_amount']
      @transaction.input_currency = transaction_data['input_currency'] if transaction_data['input_currency']
      @transaction.output_amount = transaction_data['output_amount'].to_f.round(2) if transaction_data['output_amount']
      @transaction.output_currency = transaction_data['output_currency'] if transaction_data['output_currency']
    end
    if @transaction.save
      render json: @transaction, status: :created
    else
      render json: { error: @transaction.errors.full_messages.to_sentence.downcase }, status: :unprocessable_entity
    end
  end
end