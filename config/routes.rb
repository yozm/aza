Rails.application.routes.draw do
  resources :transactions, only: [:index, :create, :show, :update]
  post 'users/generate_token'
end
